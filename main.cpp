#include <iostream>

#include "boost/filesystem.hpp"

using namespace std;
using namespace boost;

int main() {
    std::cout << "Simple project to test boost installation" << std::endl;

    cout << "current path from boost: " << filesystem::current_path() << endl;
    return 0;
}